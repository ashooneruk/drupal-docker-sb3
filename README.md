# README #

This is a dockerfile for building Drupal 7 with UK's current Drupal platform. It's a modification of https://github.com/wodby/drupal/blob/master/7/5.6/Dockerfile.


### What is modified? ###
This file makes additional changes to the default drupal-composer composer.json file that wodby uses in order to include:

* The current UK Seeblue Drupal 7 theme